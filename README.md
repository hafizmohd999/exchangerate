# ExchangeRate


1) Check a list of currency rates against any currency of the user’s choice(base currency).
2) Convert any amount from the user’s chosen base currency to a selected currency.
3) View currency rates even without an internet connection. 
Yet, still able to refresh the latest rates by tapping the refresh button.
