package com.hafiz.exchangerate.EntityObj;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.hafiz.exchangerate.GlobalVariables.Variables;

import java.util.Map;

@Entity(tableName = "rates_table")
public class CurrencyRate {  // create model class
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "dateCurrency")
    private int date;

    @ColumnInfo(name = "basedCurrency")
    private String basedCurrency;

    @ColumnInfo(name = "valueCurrency")
    private String valueRateCurrency;

    public CurrencyRate(@NonNull int date, String basedCurrency, String valueRateCurrency){
        this.date = date;
        this.basedCurrency = basedCurrency;
        this.valueRateCurrency = valueRateCurrency;
    }

    public int getDate() {
        return date;
    }

    public String getBasedCurrency() {
        return basedCurrency;
    }

    public String getValueRateCurrency() {
        return valueRateCurrency;
    }
}
