package com.hafiz.exchangerate.Db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.hafiz.exchangerate.Dao.CurrencyRateDao;
import com.hafiz.exchangerate.EntityObj.CurrencyRate;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
    1) export schema meaning that no export the database schema into a folder (set to false).
    2) @Database and use the annotation parameters to declare the entities that belong in the database and set the version number.
    3) We've defined a singleton, CurrencyRateRoomDb, to prevent having multiple instances of the database opened at the same time.
    4) getDatabase returns the singleton. It'll create the database the first time it's accessed,
    using Room's database builder to create a RoomDatabase object in the application context from the WordRoomDatabase class and names it "word_database".
    5) We've created an ExecutorService with a fixed thread pool that you will use to run database operations asynchronously on a background thread.
 */
@Database(entities = {CurrencyRate.class}, version = 2, exportSchema = false)
public abstract class CurrencyRateRoomDb extends RoomDatabase {
    public abstract CurrencyRateDao currencyDao();

    private static volatile CurrencyRateRoomDb INSTANCE;  //create instance db for the first time
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static CurrencyRateRoomDb getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CurrencyRateRoomDb.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CurrencyRateRoomDb.class, "currency_db")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
