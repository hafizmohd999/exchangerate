package com.hafiz.exchangerate.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.hafiz.exchangerate.EntityObj.CurrencyRate;

import java.util.List;

//sqlite code to insert or update table
@Dao
public interface CurrencyRateDao {
    //if date (primary key is same replace data if not same insert new data)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(CurrencyRate rate);

    @Query("SELECT * FROM rates_table")  // use live data for data observation and solve a  lifecycle
    List<CurrencyRate> getListCurrencyUpdate();

    //based on id data
    @Query("SELECT * FROM rates_table WHERE dateCurrency = :id")
    List<CurrencyRate> getSelectCurrencyUpdate(int id);
}
