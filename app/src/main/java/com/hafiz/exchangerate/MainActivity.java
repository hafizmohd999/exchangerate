package com.hafiz.exchangerate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private AppCompatButton viewListCurrency, convertCurrrency, viewCurrencyRate;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;

        viewListCurrency = findViewById(R.id.currencyList);  //user can choose based currency
        viewListCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  //intent activity to view list currency
                Intent viewCurrency = new Intent(context, ViewListCurrency.class);
                startActivity(viewCurrency);
            }
        });

        convertCurrrency = findViewById(R.id.convertCurrency);
        convertCurrrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  //intent to activity to convert currency
                Intent mconvertCurrency = new Intent(context, ConvertCurrency.class);
                startActivity(mconvertCurrency);
            }
        });

        viewCurrencyRate = findViewById(R.id.viewCurrency);  //user can't choose based currency by default europe currency again any of currency rate country
        viewCurrencyRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent defaultBased = new Intent(context, ViewCurrencyBasedEurope.class);
                startActivity(defaultBased);
            }
        });
    }
}