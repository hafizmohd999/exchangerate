package com.hafiz.exchangerate;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hafiz.exchangerate.Api.Client;
import com.hafiz.exchangerate.Api.ClientInterface;
import com.hafiz.exchangerate.EntityObj.CurrencyRate;
import com.hafiz.exchangerate.GlobalVariables.Variables;
import com.hafiz.exchangerate.Repository.CurrencyRateRepository;
import com.hafiz.exchangerate.Utility.AppsFunction;
import com.hafiz.exchangerate.Utility.ConverterHashMap;
import com.hafiz.exchangerate.Utility.DecimalDigitsInputFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConvertCurrency extends AppCompatActivity {
    private AppCompatEditText mCurrencyUser;
    private Spinner toCurrency, basedCurrency;
    private AppCompatButton convertBtn;
    private AppsFunction funct;
    private Context context;
    private ClientInterface client;
    private ArrayAdapter<String> spinnerArrayAdapter, spinnerArrayAdapter2;
    private List<String>keysList, basedList;
    private AppCompatTextView valueConvert;
    private CurrencyRateRepository repository;
    private List<CurrencyRate> currencyRateList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convert_currency);

        context = ConvertCurrency.this;
        funct = new AppsFunction();

        valueConvert = findViewById(R.id.lbl5);

        //create back button
        createBackButton();

        //user input currency and strict to only accept number decimal
        mCurrencyUser = findViewById(R.id.currency);
        mCurrencyUser.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});

        //spinner
        toCurrency = findViewById(R.id.listspin);
        basedCurrency = findViewById(R.id.listspin2);

        funct.registerNetworkCallback(context);

        //check connection if got internet get data from api else from db
        if (Variables.isNetworkConnected){
            client = Client.getClient(Variables.baseUrl, Variables.timeOut).create(ClientInterface.class);

            Call<ResponseBody> clientreq = client.getCurrencyUpdate();

            clientreq.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200){
                        try {
                            JsonObject jsonObject = new Gson().fromJson(response.body().string(), JsonObject.class);
                            // member object
                            JsonObject rates = jsonObject.get("rates").getAsJsonObject();
                            String based = jsonObject.get("base").getAsString();
                            String dateCurrency = jsonObject.get("date").getAsString();

                            Map<String, Double> rateExchangeObjMap = new HashMap<String, Double>();

                            Set<Map.Entry<String, JsonElement>> entrySet = rates.entrySet();

                            for(Map.Entry<String, JsonElement> entry : entrySet) {
                                rateExchangeObjMap.put(entry.getKey(), Double.valueOf(String.valueOf(entry.getValue())));
                            }
                            //create spinner adapter;
                            keysList = new ArrayList<>();
                            basedList = new ArrayList<>();

                            for (String name:  rateExchangeObjMap.keySet()) {
                                String key = name.toString();
                                keysList.add(key);
                                basedList.add(key);
                            }

                            //set adapter spinner
                            spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, keysList );
                            toCurrency.setAdapter(spinnerArrayAdapter);

                            spinnerArrayAdapter2 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, basedList );
                            basedCurrency.setAdapter(spinnerArrayAdapter2);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }


        //button convert
        convertBtn = findViewById(R.id.btnConvert);
        convertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrencyUser.getText().toString().isEmpty() || mCurrencyUser.getText().toString().equals("")){
                    Toast.makeText(context, "Please key in your currency to convert", Toast.LENGTH_LONG).show();
                    return;
                }
                String toCurr = toCurrency.getSelectedItem().toString();  //spinner value
                String basedCurr = basedCurrency.getSelectedItem().toString();

                double euroVlaue = Double.valueOf(mCurrencyUser.getText().toString());  //user value

                try {
                    convertCurrency(toCurr, euroVlaue, basedCurr);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void convertCurrency(final String selectedCurrencyCountry, final double valueToConvert, final String basedCurrencyItem) throws IOException {
        if (Variables.isNetworkConnected){
            client = Client.getClient(Variables.baseUrl, Variables.timeOut).create(ClientInterface.class);

            Call<ResponseBody> clientreq = client.getBasedCurrencyToSelectedCurrency(basedCurrencyItem, selectedCurrencyCountry);

            clientreq.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200){
                        try {
                            JsonObject jsonObject = new Gson().fromJson(response.body().string(), JsonObject.class);
                            // mmber object
                            JsonObject rates = jsonObject.get("rates").getAsJsonObject();

                            Set<Map.Entry<String, JsonElement>> entrySet = rates.entrySet();

                            for(Map.Entry<String, JsonElement> entry : entrySet) {
                                double output = valueToConvert * Double.valueOf(String.valueOf(entry.getValue()));
                                valueConvert.setText(String.valueOf(output));
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }/*else{  //no data get db
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    repository = new CurrencyRateRepository(context);

                    currencyRateList = repository.getmAllCurrencyRate();

                    //check if database is not empty and null
                    if (currencyRateList != null){
                        Map<String, Double> exchangeRate = null;

                        for (CurrencyRate rate: currencyRateList){
                            exchangeRate = ConverterHashMap.fromString(rate.getValueRateCurrency());
                        }

                        // check if exchangerate is not null and empty
                        if (exchangeRate != null){
                            for (String name:  exchangeRate.keySet()) {
                                String key = name.toString();

                                if (toCurr.equals(key)){
                                    double output = euroVlaue * exchangeRate.get(name).doubleValue();

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            // updates the UI
                                            valueConvert.setText(String.valueOf(output));
                                        }
                                    });
                                }
                            }
                        }
                    }else{
                        Toast.makeText(context, "Please click refresh button to get latest data", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }*/
    }

    public void createBackButton(){
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}