package com.hafiz.exchangerate;

import android.app.Application;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hafiz.exchangerate.Api.Client;
import com.hafiz.exchangerate.Api.ClientInterface;
import com.hafiz.exchangerate.EntityObj.CurrencyRate;
import com.hafiz.exchangerate.GlobalVariables.Variables;
import com.hafiz.exchangerate.Repository.CurrencyRateRepository;
import com.hafiz.exchangerate.Utility.AppsFunction;
import com.hafiz.exchangerate.Utility.ConverterHashMap;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//create based application to fetch data once time. If internet close connect to db room or shared preference
public class BasedApplication extends Application {
    private AppsFunction utility;
    private ClientInterface client;
    private CurrencyRateRepository mRepository; //fetch data from api store to local db using repository

    @Override
    public void onCreate() {
        super.onCreate();

        utility = new AppsFunction();
        //check network
        utility.registerNetworkCallback(this);

        if (Variables.isNetworkConnected){ //fetch data latest currency and store to db
            //store latest date currency rate
            client = Client.getClient(Variables.baseUrl, Variables.timeOut).create(ClientInterface.class);

            Call<ResponseBody> clientreq = client.getCurrencyUpdate();

            clientreq.enqueue(new Callback<ResponseBody>() {
                 @Override
                 public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                     if (response.code() == 200){
                         try {
                            JsonObject jsonObject = new Gson().fromJson(response.body().string(), JsonObject.class);
                             // mmber object
                             JsonObject rates = jsonObject.get("rates").getAsJsonObject();
                             String based = jsonObject.get("base").getAsString();
                             String dateCurrency = jsonObject.get("date").getAsString();

                             // create temp date convert to int type
                             int tempDate = Integer.parseInt(dateCurrency.replace("-", ""));

                             Map<String, Double> rateExchangeObjMap = new HashMap<String, Double>();

                             Set<Map.Entry<String, JsonElement>> entrySet = rates.entrySet();

                             for(Map.Entry<String, JsonElement> entry : entrySet) {
                                 rateExchangeObjMap.put(entry.getKey(), Double.valueOf(String.valueOf(entry.getValue())));
                             }
                             //store data into model class
                             CurrencyRate rate = new CurrencyRate(tempDate, based, ConverterHashMap.fromStringMap(rateExchangeObjMap));
                             //insert data to db
                             Executors.newSingleThreadExecutor().execute(new Runnable() {   // need execution thread in background for sake of assessment use MVP instead of MVVM (livemodelView+liveData)
                                 @Override
                                 public void run() {
                                     mRepository = new CurrencyRateRepository(BasedApplication.this);
                                     mRepository.insertData(rate);
                                 }
                             });
                         }catch (Exception e){
                             e.printStackTrace();
                         }
                     }
                 }

                 @Override
                 public void onFailure(Call<ResponseBody> call, Throwable t) {

                 }
             });
        }
    }
}


/*
//debuging value for checking value is store and can display
                             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                 rateExchangeObjMap.entrySet().forEach(stringDoubleEntry -> {
                                     System.out.println("key: " + stringDoubleEntry.getKey() + " Value: " + stringDoubleEntry.getValue());
                                 });
                             }else{
                                 for (String name:  rateExchangeObjMap.keySet()) {
                                     String key = name.toString();
                                     String value = rateExchangeObjMap.get(name).toString();
                                     System.out.println(key + " " + value);
                                 }
                             }
 */