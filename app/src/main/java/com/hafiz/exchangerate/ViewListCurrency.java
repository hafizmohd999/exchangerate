package com.hafiz.exchangerate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hafiz.exchangerate.Api.Client;
import com.hafiz.exchangerate.Api.ClientInterface;
import com.hafiz.exchangerate.GlobalVariables.Variables;
import com.hafiz.exchangerate.Utility.AppsFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewListCurrency extends AppCompatActivity {
    private Spinner toCurrency, basedCurrency;
    private AppCompatButton viewBtn;
    private Context context;
    private ClientInterface client;
    private ArrayAdapter<String> spinnerArrayAdapter, spinnerArrayAdapter2;
    private List<String> keysList, basedList;
    private AppsFunction funct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list_currency);

        //create back button
        createBackButton();

        context = ViewListCurrency.this;

        funct = new AppsFunction();

        //spinner
        toCurrency = findViewById(R.id.listspin);
        basedCurrency = findViewById(R.id.listspin2);

        funct.registerNetworkCallback(context);

        //check connection if got internet get data from api else from db
        if (Variables.isNetworkConnected){
            client = Client.getClient(Variables.baseUrl, Variables.timeOut).create(ClientInterface.class);

            Call<ResponseBody> clientreq = client.getCurrencyUpdate();

            clientreq.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200){
                        try {
                            JsonObject jsonObject = new Gson().fromJson(response.body().string(), JsonObject.class);
                            // member object
                            JsonObject rates = jsonObject.get("rates").getAsJsonObject();
                            String based = jsonObject.get("base").getAsString();
                            String dateCurrency = jsonObject.get("date").getAsString();

                            Map<String, Double> rateExchangeObjMap = new HashMap<String, Double>();

                            Set<Map.Entry<String, JsonElement>> entrySet = rates.entrySet();

                            for(Map.Entry<String, JsonElement> entry : entrySet) {
                                rateExchangeObjMap.put(entry.getKey(), Double.valueOf(String.valueOf(entry.getValue())));
                            }
                            //create spinner adapter;
                            keysList = new ArrayList<>();
                            basedList = new ArrayList<>();

                            for (String name:  rateExchangeObjMap.keySet()) {
                                String key = name.toString();
                                keysList.add(key);
                                basedList.add(key);
                            }

                            //set adapter spinner
                            spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, keysList );
                            toCurrency.setAdapter(spinnerArrayAdapter);

                            spinnerArrayAdapter2 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, basedList );
                            basedCurrency.setAdapter(spinnerArrayAdapter2);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
        viewBtn = findViewById(R.id.btnView);
        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewData = new Intent(context, GraphViewListCurrency.class);
                viewData.putExtra("based", basedCurrency.getSelectedItem().toString());
                viewData.putExtra("targetCountry", toCurrency.getSelectedItem().toString());
                startActivity(viewData);
            }
        });
    }

    public void createBackButton(){
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}