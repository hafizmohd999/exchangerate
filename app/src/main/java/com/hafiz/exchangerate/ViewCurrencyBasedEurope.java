package com.hafiz.exchangerate;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hafiz.exchangerate.Api.Client;
import com.hafiz.exchangerate.Api.ClientInterface;
import com.hafiz.exchangerate.EntityObj.CurrencyRate;
import com.hafiz.exchangerate.GlobalVariables.Variables;
import com.hafiz.exchangerate.Repository.CurrencyRateRepository;
import com.hafiz.exchangerate.Utility.AppsFunction;
import com.hafiz.exchangerate.Utility.ConverterHashMap;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCurrencyBasedEurope extends AppCompatActivity {
    private ListView listView;
    private AppsFunction funct;
    private Context context;
    private ClientInterface client;
    private CurrencyRateRepository repository;
    private List<CurrencyRate> currencyRateList;
    private ArrayList<String> keysList;
    private StableArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_currency_based_europe);

        context = ViewCurrencyBasedEurope.this;
        funct = new AppsFunction();

        listView = findViewById(R.id.list_item);

        createBackButton();

        funct.registerNetworkCallback(context);

        if (Variables.isNetworkConnected){
            //from api
            client = Client.getClient(Variables.baseUrl, Variables.timeOut).create(ClientInterface.class);

            Call<ResponseBody> clientreq = client.getCurrencyUpdate();

            clientreq.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        try {
                            JsonObject jsonObject = new Gson().fromJson(response.body().string(), JsonObject.class);
                            // mmber object
                            JsonObject rates = jsonObject.get("rates").getAsJsonObject();

                            Set<Map.Entry<String, JsonElement>> entrySet = rates.entrySet();
                            //store data in list string
                            keysList =  new ArrayList<>();

                            for(Map.Entry<String, JsonElement> entry : entrySet) {
                                keysList.add(entry.getKey() + " : " + String.valueOf(entry.getValue()));
                            }
                            //create adapter
                            adapter = new StableArrayAdapter(context,
                                    android.R.layout.simple_list_item_1, keysList);
                            //pass adapter to listview
                            listView.setAdapter(adapter);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }else{
            //get 7 days before
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DATE, -1);

            Date dateBefore1Days = cal.getTime();

            //load from db for today date
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    repository = new CurrencyRateRepository(context);
                    currencyRateList = repository.getSelectedCurrency(funct.getFormatDateInt(dateBefore1Days));

                    //check if database is not empty and null
                    if (currencyRateList.size() != 0){
                        Map<String, Double> exchangeRate = null;

                        for (CurrencyRate rate: currencyRateList){
                            exchangeRate = ConverterHashMap.fromString(rate.getValueRateCurrency());
                        }
   //20210325
                        //create data;
                        keysList = new ArrayList<>();

                        // check if exchangerate is not null and empty
                        if (exchangeRate != null){
                            for (Map.Entry<String, Double> data:   exchangeRate.entrySet()) {
                                keysList.add(data.getKey()+ " : " + String.valueOf(data.getValue()));
                            }
                            runOnUiThread(new Runnable() {  //update ui
                                @Override
                                public void run() {
                                    // Stuff that updates the UI
                                    adapter = new StableArrayAdapter(context,
                                            android.R.layout.simple_list_item_1, keysList);
                                    //pass adapter to listview
                                    listView.setAdapter(adapter);
                                }
                            });

                        }
                    }
                    else{
                        runOnUiThread(new Runnable() {  //update ui
                            @Override
                            public void run() {
                                // Stuff that updates the UI
                                Toast.makeText(context, "Database is empty for today date. Please click refresh button", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }
            });
        }
    }

    //create adapter string array for listview
    private class StableArrayAdapter extends ArrayAdapter<String> {
        //add string and auto id for each row listview
        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    public void createBackButton(){
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Refersh data
        if (id == R.id.action_name) {
            if (funct == null){
                funct = new AppsFunction();
            }

            funct.registerNetworkCallback(context);

            if (Variables.isNetworkConnected){
                //fetch data latest currency and store to db
                //store latest date currency rate
                client = Client.getClient(Variables.baseUrl, Variables.timeOut).create(ClientInterface.class);

                Call<ResponseBody> clientreq = client.getCurrencyUpdate();

                clientreq.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200){
                            try {
                                JsonObject jsonObject = new Gson().fromJson(response.body().string(), JsonObject.class);
                                // mmber object
                                JsonObject rates = jsonObject.get("rates").getAsJsonObject();
                                String based = jsonObject.get("base").getAsString();
                                String dateCurrency = jsonObject.get("date").getAsString();

                                // create temp date convert to int type
                                int tempDate = Integer.parseInt(dateCurrency.replace("-", ""));

                                Map<String, Double> rateExchangeObjMap = new HashMap<String, Double>();

                                Set<Map.Entry<String, JsonElement>> entrySet = rates.entrySet();

                                for(Map.Entry<String, JsonElement> entry : entrySet) {
                                    rateExchangeObjMap.put(entry.getKey(), Double.valueOf(String.valueOf(entry.getValue())));
                                }
                                //store data into model class
                                CurrencyRate rate = new CurrencyRate(tempDate, based, ConverterHashMap.fromStringMap(rateExchangeObjMap));
                                //insert data to db
                                Executors.newSingleThreadExecutor().execute(new Runnable() {   // need execution thread in background for sake of assessment use MVP instead of MVVM (livemodelView+liveData)
                                    @Override
                                    public void run() {
                                        repository = new CurrencyRateRepository(context);
                                        repository.insertData(rate);

                                        currencyRateList = repository.getmAllCurrencyRate();  // get back data update at db

                                        //check if database is not empty and null
                                        if (currencyRateList != null) {
                                            Map<String, Double> exchangeRate = null;

                                            for (CurrencyRate rate : currencyRateList) {
                                                exchangeRate = ConverterHashMap.fromString(rate.getValueRateCurrency());
                                            }

                                            keysList = new ArrayList<>();

                                            for (Map.Entry<String, Double> data:exchangeRate.entrySet()){
                                                keysList.add(data.getKey()+ " : " + String.valueOf(data.getValue()));
                                            }

                                            runOnUiThread(new Runnable() {      //update spinner also
                                                @Override
                                                public void run() {
                                                    //create spinner adapter;

                                                    adapter = new StableArrayAdapter(context,
                                                            android.R.layout.simple_list_item_1, keysList);
                                                    //pass adapter to listview
                                                    listView.setAdapter(adapter);
                                                    // updates the UI
                                                    Toast.makeText(context, "Done update", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }

                                    }
                                });
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
            else{
                Toast.makeText(context, "Please open the internet!", Toast.LENGTH_SHORT).show();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}