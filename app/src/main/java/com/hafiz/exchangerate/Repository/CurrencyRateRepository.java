package com.hafiz.exchangerate.Repository;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.LiveData;

import com.hafiz.exchangerate.Dao.CurrencyRateDao;
import com.hafiz.exchangerate.Db.CurrencyRateRoomDb;
import com.hafiz.exchangerate.EntityObj.CurrencyRate;

import java.util.List;
import java.util.concurrent.Executors;

/*
  1) A Repository class abstracts access to multiple data sources.
  2) A Repository class provides a clean API for data access to the rest of the application.
 3) A Repository manages queries and allows you to use multiple backends. In the most common example,
 the Repository implements the logic for deciding whether to fetch data from a network or use results cached in a local database.
 */
public class CurrencyRateRepository {
    private CurrencyRateDao mCurrencyDao;
    private List<CurrencyRate> mAllCurrencyRate;

    public CurrencyRateRepository(Context application){
        CurrencyRateRoomDb db = CurrencyRateRoomDb.getDatabase(application); //create instance db for first time
        mCurrencyDao = db.currencyDao();
        mAllCurrencyRate = mCurrencyDao.getListCurrencyUpdate();
    }

    public List<CurrencyRate> getmAllCurrencyRate(){
        return mAllCurrencyRate;
    }
    /*
    We need to not run the insert on the main thread, so we use the ExecutorService we created in the WordRoomDatabase to perform the insert on a background thread.
     */
    public void insertData(CurrencyRate rate){
        CurrencyRateRoomDb.databaseWriteExecutor.execute(() -> {
            mCurrencyDao.insert(rate);
        });
    }

    public List<CurrencyRate> getSelectedCurrency(int id){
        return mCurrencyDao.getSelectCurrencyUpdate(id);
    }
}
