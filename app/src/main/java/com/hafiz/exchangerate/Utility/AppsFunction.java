package com.hafiz.exchangerate.Utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.hafiz.exchangerate.GlobalVariables.Variables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AppsFunction {  //preparing general utilities function
    // Network Check
    public void registerNetworkCallback(Context context)
    {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnectedToWifi = false;
        boolean isConnectedToCellular = false;

        if (connectivityManager == null) {  // connection cannot instance
            //Log.e(TAG, "Connectivity manager not found!");
            Variables.isNetworkConnected = false;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network activeNetwork = connectivityManager.getActiveNetwork();
            if (activeNetwork == null) {  // no connection found
               Variables.isNetworkConnected = false;
            } else {
                NetworkCapabilities nc = connectivityManager.getNetworkCapabilities(activeNetwork);
                if (nc == null) {  // no connection found
                    Variables.isNetworkConnected = false;
                } else { // check type of connection
                    isConnectedToCellular = nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR);
                    isConnectedToWifi = nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
                }
            }
        } else {  //check lower api 23
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null) {
                if (networkInfo.isConnectedOrConnecting()) {
                    isConnectedToCellular =
                            networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
                    isConnectedToWifi =
                            networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
                } else {  // no found internet type
                   Variables.isNetworkConnected = false;
                }
            } else { // network info return empty
                Variables.isNetworkConnected = false;
            }
        }

        if (isConnectedToCellular && isConnectedToWifi) { // if cellular and wifi found set network true
            //return NetworkType.WIFI;
            Variables.isNetworkConnected = true;
        } else if (isConnectedToCellular) {  // connect cellular only
            Variables.isNetworkConnected = true;
        } else if (isConnectedToWifi) {  // connect wifi only
            Variables.isNetworkConnected = true;
        } else {  // no connection found
            Variables.isNetworkConnected = false;
        }
    }

    // return format interget date like this 20210324
    public int getFormatDateInt (Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        return Integer.parseInt(formatter.format(date));
    }

    //convert interger date into string date format  //20210323
    public String getFormatDatetoString(int date){
        String temp = Integer.toString(date);
        return temp.substring(0,4) + "-" + temp.substring(4,6) + "-" + temp.substring(6, temp.length());
    }

    public String getFormateDateString(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }
}
