package com.hafiz.exchangerate.Utility;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

//create class utilies to convert hashmap to string and string to hashmap

public class ConverterHashMap {

    @TypeConverter
    public static Map<String, Double> fromString(String value){
        Type mapType = new TypeToken<Map<String, Double>>(){
        }.getType();

        return new Gson().fromJson(value, mapType);
    }

    @TypeConverter
    public static String fromStringMap(Map<String, Double> map){
        Gson gson = new Gson();
        return  gson.toJson(map);
    }
}
