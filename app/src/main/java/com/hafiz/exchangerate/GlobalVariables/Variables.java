package com.hafiz.exchangerate.GlobalVariables;

public class Variables {
    // Global variable used to store network state
    public static boolean isNetworkConnected = false;
    //base Api
    public static String baseUrl = "https://api.exchangeratesapi.io/";
    //time out call api
    public static long timeOut= 60;
}
