package com.hafiz.exchangerate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hafiz.exchangerate.Api.Client;
import com.hafiz.exchangerate.Api.ClientInterface;
import com.hafiz.exchangerate.GlobalVariables.Variables;
import com.hafiz.exchangerate.Utility.AppsFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//https://medium.com/@clyeung0714/using-mpandroidchart-for-android-application-barchart-540a55b4b9ef
public class GraphViewListCurrency extends AppCompatActivity {
    private String basedCountry, targetCountry;
    private ClientInterface client;
    private Context context;
    private AppsFunction funct;
    private BarChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_view_list_currency);

        context = GraphViewListCurrency.this;
        funct = new AppsFunction();

        //get data intent
        basedCountry = getIntent().getStringExtra("based");
        targetCountry = getIntent().getStringExtra("targetCountry");

        chart = findViewById(R.id.chart1);

        //call api
        funct.registerNetworkCallback(context);

        if (Variables.isNetworkConnected){
            //get 7 days before
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DATE, -7);

            Date dateBefore7Days = cal.getTime();

            client = Client.getClient(Variables.baseUrl, Variables.timeOut).create(ClientInterface.class);

            Call<ResponseBody> clientreq = client.getHistoryPass7days(funct.getFormateDateString(dateBefore7Days), funct.getFormateDateString(new Date()), basedCountry, targetCountry);

            clientreq.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200){
                        try {
                            JsonObject jsonObject = new Gson().fromJson(response.body().string(), JsonObject.class);
                            // member object
                            JsonObject rates = jsonObject.get("rates").getAsJsonObject();

                            Set<Map.Entry<String, JsonElement>> entrySet = rates.entrySet();

                            //setup data barchart
                            String titleBarLegend = "";  //create barchart indicator name

                            ArrayList<String> xVals = new ArrayList<String>();  //set value x-axis to date of data
                            ArrayList<BarEntry> barEntryArrayList = new ArrayList<BarEntry>(); // set entry object barchart
                            ArrayList<Double> valueYaxis = new ArrayList<>();     // set y-axis

                            for(Map.Entry<String, JsonElement> entry : entrySet) {
                               //System.out.println("Key: " + entry.getKey() + " Value: " +entry.getValue());
                               xVals.add(entry.getKey()); // add date as x-Axis indicator

                               JsonObject valuer = entry.getValue().getAsJsonObject();

                               Set<Map.Entry<String, JsonElement>> entryKeyValue = valuer.entrySet();

                               for (Map.Entry<String,JsonElement> entryValue : entryKeyValue){
                                   titleBarLegend = entryValue.getKey();

                                   JsonElement elementValue = entryValue.getValue();

                                   valueYaxis.add(elementValue.getAsDouble());  //add value barchart
                               }
                            }

                            //fit the data into a bar
                            for (int i = 0; i < valueYaxis.size(); i++) {
                                BarEntry barEntry = new BarEntry(i, valueYaxis.get(i).floatValue());  //entity of single object barchart
                                barEntryArrayList.add(barEntry); //add bar entry
                            }

                            //show barchart : set data object graph and showing in the graph
                            BarDataSet barDataSet = new BarDataSet(barEntryArrayList, titleBarLegend);
                            //set properties of dataset like color and so on.
                            //Changing the color of the bar
                            barDataSet.setColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));
                            //Setting the size of the form in the legend
                            barDataSet.setFormSize(15f);
                            //showing the value of the bar, default true if not set
                            barDataSet.setDrawValues(true);
                            //setting the text size of the value of the bar
                            barDataSet.setValueTextSize(12f);

                            BarData data = new BarData(barDataSet);

                            //set properties chart
                            //hiding the grey background of the chart, default false if not set
                            chart.setDrawGridBackground(true);
                            //remove the bar shadow, default false if not set
                            chart.setDrawBarShadow(true);
                            //remove border of the chart, default false if not set
                            chart.setDrawBorders(false);

                            //create description of the barchart
                            Description description = new Description();
                            description.setEnabled(true);
                            description.setText("Selected currency is " +targetCountry+" against base currency is " +
                                            basedCountry +" rates up to 7 days");
                            description.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                            //description.setTextSize(10f);
                            chart.setDescription(description);

                            //set chart data and show
                            chart.setData(data);
                            chart.invalidate();

                            XAxis xAxis = chart.getXAxis();  //set X-axis to date
                            xAxis.setValueFormatter(new ValueFormatter() {
                                @Override
                                public String getFormattedValue(float value) {
                                    return xVals.get((int) value);
                                }
                            });
                            //change the position of x-axis to the bottom
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            //set the horizontal distance of the grid line
                            xAxis.setGranularity(1f);
                            //hiding the x-axis line, default true if not set
                            xAxis.setDrawAxisLine(false);
                            //hiding the vertical grid lines, default true if not set
                            xAxis.setDrawGridLines(false);

                            YAxis leftAxis = chart.getAxisLeft();
                            //hiding the left y-axis line, default true if not set
                            leftAxis.setDrawAxisLine(false);

                            YAxis rightAxis = chart.getAxisRight();
                            //hiding the right y-axis line, default true if not set
                            rightAxis.setDrawAxisLine(false);

                            Legend legend = chart.getLegend();
                            //setting the shape of the legend form to line, default square shape
                            legend.setForm(Legend.LegendForm.LINE);
                            //setting the text size of the legend
                            legend.setTextSize(11f);
                            //setting the alignment of legend toward the chart
                            legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                            legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                            //setting the stacking direction of legend
                            legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                            //setting the location of legend outside the chart, default false if not set
                            legend.setDrawInside(false);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }

        //show data in graph
        createBackButton();
    }

    public void createBackButton(){
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}