package com.hafiz.exchangerate.Api;

import com.google.gson.JsonObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ClientInterface {  // create interface api
    @GET("latest")  //latest current date to convert data  & use db if no internet
    Call<ResponseBody> getCurrencyUpdate();

    //history of 7 days
    //format date 2021-03-17
    @GET("history") //&base=EUR&symbols=MYR // use for list currency
    Call<ResponseBody>getHistoryPass7days(@Query("start_at") String oldDay, @Query("end_at") String newDay,
                                          @Query("base") String basedCurrency, @Query("symbols") String symbols);

    @GET("latest")  // use in converter
    Call<ResponseBody> getBasedCurrencyToSelectedCurrency(@Query("base") String based, @Query("symbols") String targetCountry);
}
